import { Component } from '@angular/core';
import { MoviesService } from '../movies.service';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent {
  title = 'app';
  movies = [];
  formError = false;

  // instancie le l'objet du service
  constructor(private moviesService: MoviesService,
             private service: NotificationsService) {}

  // Quand le DOM est chargé recupere le tableau du service 'Moviesservice'
  ngOnInit() {
    this.moviesService.getpopularsMovies().subscribe(
      (data: any) => {
      this.movies = data.results;
      // console.log(this.movies);
    },
    (err: any) => {
      console.log(err);
    } )
  }

  search = ( param_tex_search ) => {
    event.preventDefault(); // annule le comportement par défaut
    console.log('cherche');
    console.log(param_tex_search.value);
    if (param_tex_search.value === '') {
      this.formError = true;
       this.service.error(
       'Erreur!!',
       'Le champ est vide',
       {
        timeOut: 5000,
        showProgressBar: true,
        pauseOnHover: false,
        clickToClose: true,
        maxLength: 10,
        position: 0
    }
      );
      return true;
    }else{
      this.formError = false;
    }
    this.moviesService.getMovieSearch(param_tex_search.value).subscribe(
      (data: any) => {
        this.movies = data.results;
        console.log(this.movies);

      },
      (err: any) => {
        console.log(err);
        this.formError = true;
      },
      () => {
        console.log('complete');
       // param_tex_search.value = '';
      }
    );
  }

 

}
