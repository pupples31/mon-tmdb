import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../movies.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {

  movies = {};
  constructor(private movieService: MoviesService, private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    this.movieService.getMovieById(id).subscribe(
    (data: any) => {
    this.movies = data;
    console.log(data);
    },
    (err: any) => {
      console.log(err);
    } )
  }
}
