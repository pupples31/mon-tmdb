import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes} from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { MovieListComponent } from './movie-list/movie-list.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { MovieDisplayComponent } from './movie-display/movie-display.component';


@NgModule({
  declarations: [
    AppComponent,
    MovieListComponent,
    NotFoundComponent,
    MovieDetailComponent,
    MovieDisplayComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule, 
    SimpleNotificationsModule.forRoot(),
    BrowserAnimationsModule,
    RouterModule.forRoot([
    {path: 'movie-list', component: MovieListComponent},    
    {path: '', redirectTo: '/movie-list', pathMatch: 'full'},
    {path: 'movie-detail/:id', component: MovieDetailComponent},
    {path: '**', component: NotFoundComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
