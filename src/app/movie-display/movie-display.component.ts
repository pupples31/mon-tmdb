import { Component, OnInit, OnChanges, Input } from '@angular/core';


@Component({
  selector: 'app-movie-display',
  templateUrl: './movie-display.component.html',
  styleUrls: ['./movie-display.component.css']
})
export class MovieDisplayComponent implements OnInit, OnChanges {

  @Input() moviesInput;
  @Input() input1;
  @Input() input2;

  constructor() { }

  ngOnInit() {

  }

  ngOnChanges() {
    console.log(this.moviesInput);
    console.log(this.input1);
    console.log(this.input2);
  }

}
