import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class MoviesService {

  // clé pour l'API
  api_key =  '00afc4a28a149e54c9015f55b7f1ad15';

  // instanncie l'objet httpClientModule
  constructor(private http: HttpClient) { }

  getpopularsMovies() {
    const uri_popular = 'https://api.themoviedb.org/3/discover/movie?api_key=' + this.api_key +'&sort_by=popularity.desc&include_adult=false&page=1';
    return this.http.get(uri_popular);
  }

  getMovieById(id) {
    const uri_movie = 'https://api.themoviedb.org/3/movie/'+ id +'?api_key=' + this.api_key;
    return this.http.get(uri_movie);
  }

  // https://api.themoviedb.org/3/search/movie?api_key={api_key}&query=Jack+Reacher
  getMovieSearch(param_tex) {
    const uri_search = 'https://api.themoviedb.org/3/search/movie?api_key=' + this.api_key +'&query=' + param_tex + '&page=1&include_adult=false';
    return this.http.get(uri_search);
  }

}
